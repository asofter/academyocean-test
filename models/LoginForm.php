<?php
namespace app\models;

use yii\base\Model;
use Yii;

class LoginForm extends Model
{
    public $username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'required'],
            ['username', 'string', 'max' => 150]
        ];
    }

    public function save()
    {
        if ($this->validate()) {
            $user = User::create($this->getAttributes());
            if($user) {
                Yii::$app->getUser()->login($user);
            }
        }

        return false;
    }

    public function scenarios() {
        return [
            'default' => [
                'username'
            ]
        ];
    }

}
