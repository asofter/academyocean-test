<?php
namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class UserLessons extends ActiveRecord
{

    public function rules()
    {
        return [
            [['started_at', 'finished_at', 'points', 'user_id'], 'integer'],
            ['user_id', 'unique'],
            ['points', 'default', 'value' => 0]
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function startLesson($user_id) {
        $model = new self();
        $model->user_id = $user_id;
        $model->started_at = time();
        $model->finished_at = 0;
        $model->points = 0;
        return $model->save(false);
    }

    public static function finishLesson($user_id) {
        $model = self::findOne(['user_id' => $user_id, 'finished_at' => 0]);
        if(!$model) {
            return false;
        }
        $model->finished_at = time();
        return $model->save(false);
    }

    public static function addPoints($user_id, $points) {
        $model = self::findOne(['user_id' => $user_id, 'finished_at' => 0]);
        if(!$model) {
            return false;
        }
        $model->points += $points;
        return $model->save(false);
    }

}
