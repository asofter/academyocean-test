<?php

use yii\db\Schema;
use yii\db\Migration;

class m161101_072420_user_lessons extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('user_lessons', [
            'id'                   => 'pk',
            'user_id'           => Schema::TYPE_INTEGER . ' NOT NULL',
            'started_at'           => Schema::TYPE_INTEGER . ' NOT NULL',
            'finished_at'           => Schema::TYPE_INTEGER . ' NOT NULL',
            'points'           => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('user_lessons');
    }
}
