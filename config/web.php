<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log'
    ],
    'components' => [
        'request' => [
            'enableCsrfValidation' => false,
            'cookieValidationKey' => 'BBBB1111BBBB',
        ],
        /*
         * You can add different types of tests:
         * - text - plain text which you can read and earn points
         * - custom - your custom code
         * - youtube - watch video till the end
         * - vars_math - some math action between two fields
         * - radio - choose some radio button
         */
        'test' => [
            'class' => 'app\components\Test',
            'lessons' => [
                1 => [
                    'type' => 'text',
                    'points' => 1,
                    'text' => "Lorem ipsum dolor sit amet, commodo ultrices nulla eget varius. Ut vehicula porta aliquam potenti. Molestie quam sed consectetuer velit rhoncus, vel ullamcorper, suspendisse duis quisque, elit diam id sed molestie et id, at adipiscing vehicula. Suscipit consectetuer sed arcu sit praesent et, purus cursus dignissim egestas vestibulum tincidunt, cras platea. Porttitor senectus lacus tortor urna congue sed, fames pellentesque vel dapibus vitae, nibh per non semper, mauris leo dui enim. Justo sit risus. Non quisque iaculis ante id. Eu eget facilisis ridiculus mi eget, at sapien magna a diam, vivamus lacinia bibendum ut habitant et vel, lectus auctor cum integer venenatis voluptas. Amet vel."
                ],
                2 => [
                    'type' => 'vars_math',
                    'action' => '+',
                    'points' => 1
                ],
                3 => [
                    'type' => 'custom',
                    'action' => 'programming_languages',
                    'points' => 1
                ],
                4 => [
                    'type' => 'radio',
                    'points' => 1,
                    'label' => 'What day of the week today?',
                    'choices' => [
                        1 => 'Monday',
                        2 => 'Tuesday',
                        3 => 'Wednesday',
                        4 => 'Thursday',
                        5 => 'Friday',
                        6 => 'Saturday',
                        7 => 'Sunday'
                    ],
                    'result' => date("N")
                ],
                5 => [
                    'type' => 'youtube',
                    'videoId' => 'IZ6jD6l_lMU',
                    'points' => 1
                ]
            ]
        ],
        'cache' => [
            'class' => 'yii\caching\MemCache',
            'keyPrefix' => 'lesson_',
            'servers' => [
                [
                    'host' => 'localhost',
                    'port' => 11211,
                    'weight' => 100,
                ]
            ]
        ],
        /*'cache' => [
            'class' => 'yii\caching\FileCache',
        ],*/
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['/site/index']
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager'=> [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'suffix' => '/',
            'rules' =>  [
                '/'=>'site/index',

                '<controller:\w+>/<action:[0-9a-zA-Z_\-]+>'                       => '<controller>/<action>',
                '<controller:\w+>'                                                => '<controller>/index',
            ]
        ]
    ],
    'params' => $params,
];

if(YII_DEBUG) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];
}

return $config;
