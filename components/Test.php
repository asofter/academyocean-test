<?php
namespace app\components;

use app\models\UserLessons;
use Yii;
use yii\base\Object;
use yii\filters\PageCache;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

class Test extends Object {
    const LESSON_ID_SESSION_NAME = 'lesson_id';
    const WATCH_VIDEO_SESSION_NAME = 'watch_video';

    public $lessons;
    public $currentLesson = null;

    public function start() {
        $firstLessonId = key($this->getLessons());
        $this->setCurrentLessonId($firstLessonId);

        UserLessons::startLesson(Yii::$app->getUser()->getId());

        Yii::$app->getResponse()->redirect(['/site/test'])->send();
        return;
    }

    public function finish() {
        UserLessons::finishLesson(Yii::$app->getUser()->getId());

        Yii::$app->getResponse()->redirect(['/site/finish'])->send();
        return;
    }

    public function flush() {
        $this->setCurrentLessonId(null);
        Yii::$app->session->set('watch_video', null);
        Yii::$app->getUser()->logout();
    }

    public function getLessons() {
        return $this->lessons;
    }

    public function getCurrentLessonId() {
        return Yii::$app->session->get(self::LESSON_ID_SESSION_NAME);
    }

    public function setCurrentLessonId($id) {
        $lessons = $this->getLessons();
        if(!isset($lessons[$id])) {
            return false;
        }

        Yii::$app->session->set(self::LESSON_ID_SESSION_NAME, $id);

        return true;
    }

    public function getCurrentLesson() {
        if(!is_null($this->currentLesson)) {
            return $this->currentLesson;
        }

        $id = $this->getCurrentLessonId();
        $lessons = $this->getLessons();

        if(!isset($lessons[$id])) {
            return false;
        }

        $this->currentLesson = ArrayHelper::merge(['id' => $id], $lessons[$id]);

        return $this->currentLesson;
    }

    public function goToNextLesson() {
        $id = $this->getCurrentLessonId()+ 1;
        $this->currentLesson = null;
        if(!$this->setCurrentLessonId($id)) {
            $this->finish();
        }
    }

    public function calculateTimeSpan($dateTimeFinished, $dateTimeStarted){
        $seconds  = $dateTimeFinished - $dateTimeStarted;

        $months = floor($seconds / (3600*24*30));
        $day = floor($seconds / (3600*24));
        $hours = floor($seconds / 3600);
        $mins = floor(($seconds - ($hours*3600)) / 60);
        $secs = floor($seconds % 60);

        if($seconds < 60)
            $time = $secs." seconds";
        else if($seconds < 60*60 )
            $time = $mins." min";
        else if($seconds < 24*60*60)
            $time = $hours." hours";
        else if($seconds < 24*60*60)
            $time = $day." day";
        else
            $time = $months." month";

        return $time;
    }

    public function getTopList($count = 10) {
        $result = [];
        $i = 0;

        $userLessons = UserLessons::find()->where("finished_at!=0")->orderBy(['points' => SORT_DESC])->limit($count)->all();
        foreach($userLessons as $userLesson) {
            $i++;
            $result[] = [
                'id' => $i,
                'username' => $userLesson->user->username,
                'points' => $userLesson['points'],
                'time' => $this->calculateTimeSpan($userLesson['finished_at'], $userLesson['started_at'])
            ];
        }

        return $result;
    }

    public function processResults() {
        if(Yii::$app->request->isPost && isset($_POST['complete-test'])) {
            $lesson = $this->getCurrentLesson();

            switch ($lesson['type']) {
                case "text":
                    UserLessons::addPoints(Yii::$app->getUser()->getId(), $lesson['points']);

                    break;
                case "vars_math":
                    $var1 = intval(Yii::$app->request->post('var1'));
                    $var2 = intval(Yii::$app->request->post('var2'));
                    $result = Yii::$app->request->post('result');
                    $action = $lesson['action'];

                    if($action == '+') {
                        $computedResult = $var1 + $var2;
                    } elseif($action == '-') {
                        $computedResult = $var1 - $var2;
                    } elseif($action == '/') {
                        $computedResult = $var1 / $var2;
                    } elseif($action == '*' || $action == 'x') {
                        $computedResult = $var1 * $var2;
                    } else {
                        throw new HttpException(403, "Can't recognize the action.");
                    }

                    if($computedResult == $result) {
                        UserLessons::addPoints(Yii::$app->getUser()->getId(), $lesson['points']);
                    }

                    break;
                case "custom":
                    if($lesson['action'] == 'programming_languages') {
                        $result = $_POST['result'];
                        if(count($result) > 0 && !in_array('Visual Basic', $result)) {
                            UserLessons::addPoints(Yii::$app->getUser()->getId(), $lesson['points']);
                        }
                    }

                    break;
                case "youtube":
                    if(Yii::$app->session->get(self::WATCH_VIDEO_SESSION_NAME) == 1) {
                        UserLessons::addPoints(Yii::$app->getUser()->getId(), $lesson['points']);
                    }

                    break;
                case "radio":
                    $result = Yii::$app->request->post('result');
                    $goodResult = $lesson['result'];

                    if($goodResult == $result) {
                        UserLessons::addPoints(Yii::$app->getUser()->getId(), $lesson['points']);
                    }

                    break;
                default:
                    throw new HttpException(403, "This type of lesson is not supported.");
            }

            $this->goToNextLesson();
        }
    }

    public function processView() {
        $lesson = Yii::$app->test->getCurrentLesson();

        $view = '';
        $tplVars = [];
        switch ($lesson['type']) {
            case "text":
                $view = 'test/text';
                $tplVars = [
                    'text' => $lesson['text']
                ];

                break;
            case "vars_math":
                $view = 'test/vars_math';
                $tplVars = [
                    'action' => $lesson['action'],
                    'var1' => mt_rand(10, 99),
                    'var2' => mt_rand(10, 99)
                ];

                break;
            case "custom":
                if($lesson['action'] == 'programming_languages') {
                    $view = 'test/custom_pl';
                }

                break;
            case "youtube":
                $view = 'test/youtube';
                $tplVars = [
                    'videoId' => $lesson['videoId']
                ];

                break;
            case "radio":
                $view = 'test/radio';
                $tplVars = [
                    'choices' => $lesson['choices'],
                    'label' => $lesson['label']
                ];

                break;
            default:
                throw new HttpException(403, "This type of lesson is not supported.");
        }

        if(empty($view)) {
            throw new HttpException(403, "There is no view to show.");
        }

        return ['view' => $view, 'tplVars' => $tplVars];
    }

}