<?php

namespace app\controllers;

use app\components\Test;
use app\models\User;
use app\models\UserLessons;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\web\HttpException;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['test', 'finish', 'watch-video'],
                'rules' => [
                    [
                        'actions' => ['test', 'finish', 'watch-video'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        Yii::$app->test->flush();

        $loginForm = new User();
        if(Yii::$app->request->isPost && $loginForm->load($_POST)) {
            if($loginForm->validate()) {
                $user = User::create($loginForm->getAttributes());
                if($user) {
                    Yii::$app->getUser()->login($user);
                    Yii::$app->test->start();
                }
            }
        }

        $topList = Yii::$app->test->getTopList();

        return $this->render('index', [
            'loginForm' => $loginForm,
            'topList' => $topList
        ]);
    }

    public function actionTest() {
        Yii::$app->test->processResults();

        $lesson = Yii::$app->test->getCurrentLesson();
        if(!$lesson) {
            throw new HttpException(403, "No lessons found");
        }

        $result = Yii::$app->test->processView();
        $view = $result['view'];
        $tplVars = $result['tplVars'];

        return $this->render($view, ArrayHelper::merge(
            ['lessonId' => $lesson['id']],
            $tplVars
        ));
    }

    public function actionFinish() {
        $userLesson = UserLessons::findOne(['user_id' => Yii::$app->getUser()->getId()]);
        if($userLesson['finished_at'] == 0) {
            throw new HttpException(404);
        }

        if(Yii::$app->request->isPost && isset($_POST['go-to-first-page'])) {
            return $this->redirect(['/site/index']);
        }

        return $this->render('finish', [
            'username' => Yii::$app->getUser()->identity->username,
            'points' => $userLesson['points'],
            'time' => Yii::$app->test->calculateTimeSpan($userLesson['finished_at'], $userLesson['started_at'])
        ]);
    }

    public function actionWatchVideo() {
        $lesson = Yii::$app->test->getCurrentLesson();
        if(!$lesson) {
            throw new HttpException(403, "No lessons found");
        }

        if($lesson['type'] == 'youtube') {
            Yii::$app->session->set(Test::WATCH_VIDEO_SESSION_NAME, 1);
        }
    }

}
