<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/* @var $this yii\web\View */

$this->title = 'Congratulations!';
?>

<div class="jumbotron">
    <div class="container">
        <h1>Congratulations, <?= $username; ?>!</h1>

        <p>You have earned <span class="label label-success"><?= $points; ?> points</span>.</p>
        <p>Total time you have spend on learning <span class="label label-primary"><?= $time; ?></span>.</p>

        <?php $form = ActiveForm::begin(); ?>
        <p><?= Html::submitButton('Go To The First Page', ['class' => 'btn btn-primary btn-lg', 'name' => 'go-to-first-page']) ?></p>
        <?php ActiveForm::end(); ?>
    </div>
</div>
