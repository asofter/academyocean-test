<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/* @var $this yii\web\View */

$this->title = 'Learning Process - AcademyOcean';
?>

<div class="jumbotron">
    <div class="container">
        <h1>Lesson #<?= $lessonId; ?></h1>

        <p>Please, watch the video till the end.</p>

        <div id="player"></div>
        <script src="http://www.youtube.com/player_api"></script>
        <script>

            // create youtube player
            var player;
            function onYouTubePlayerAPIReady() {
                player = new YT.Player('player', {
                    height: '390',
                    width: '640',
                    autoplay: 1,
                    videoId: '<?= $videoId; ?>',
                    events: {
                        'onReady': onPlayerReady,
                        'onStateChange': onPlayerStateChange
                    }
                });
            }

            // autoplay video
            function onPlayerReady(event) {
                event.target.playVideo();
            }

            // when video ends
            function onPlayerStateChange(event) {
                if(event.data === 0) {
                    $('#player').remove();

                    $.ajax({
                        type: 'POST',
                        url: '/site/watch-video/',
                        success: function(res) { }
                    });
                }
            }

        </script>

        <?php $form = ActiveForm::begin(); ?>
        <p><?= Html::submitButton('Next', ['class' => 'btn btn-primary btn-lg', 'name' => 'complete-test']) ?></p>
        <?php ActiveForm::end(); ?>
    </div>
</div>
