<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/* @var $this yii\web\View */

$this->title = 'Learning Process - AcademyOcean';
?>

<div class="jumbotron">
    <div class="container">
        <h1>Lesson #<?= $lessonId; ?></h1>

        <p>Please, solve the equation below.</p>

        <div class="well"><?= $var1; ?> <?= $action; ?> <?= $var2; ?></div>

        <?php $form = ActiveForm::begin(); ?>
        <?php
        echo Html::hiddenInput('var1', $var1);
        echo Html::hiddenInput('var2', $var2);
        ?>
        <p><?= Html::textInput('result', '', ['class' => 'form-control input-lg', 'placeholder' => 'Result']); ?></p>
        <p><?= Html::submitButton('Next', ['class' => 'btn btn-primary btn-lg', 'name' => 'complete-test']) ?></p>
        <?php ActiveForm::end(); ?>
    </div>
</div>
