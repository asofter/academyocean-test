<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/* @var $this yii\web\View */

$this->title = 'Learning Process - AcademyOcean';
?>

<div class="jumbotron">
    <div class="container">
        <h1>Lesson #<?= $lessonId; ?></h1>

        <p><?= $text; ?></p>

        <?php $form = ActiveForm::begin(); ?>
        <p><?= Html::submitButton('Next', ['class' => 'btn btn-primary btn-lg', 'name' => 'complete-test']) ?></p>
        <?php ActiveForm::end(); ?>
    </div>
</div>
