<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/* @var $this yii\web\View */

$this->title = 'AcademyOcean';
?>

<div class="jumbotron">
    <div class="container">
        <h1>Let's start learning!</h1>

        <?php $form = ActiveForm::begin([
            'id' => 'login-form'
        ]); ?>
        <?= $form->field($loginForm, 'username')->label(false)->textInput(['autofocus' => true, 'placeholder' => 'Enter your name', 'class' => 'form-control input-lg']) ?>
        <p><?= Html::submitButton('Start', ['class' => 'btn btn-primary btn-lg', 'name' => 'login-button']) ?></p>
        <?php ActiveForm::end(); ?>
    </div>
</div>

<div class="container">
    <h2>Top Learners</h2>
    <?php if(count($topList) > 0): ?>
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th style="width:40px;" class="text-center">&nbsp;</th>
                <th>Name</th>
                <th>Points</th>
                <th>Time</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($topList as $user): ?>
                <tr>
                    <td class="text-center"><?= $user['id']; ?></td>
                    <td><?= $user['username']; ?></td>
                    <td><span class="label label-primary"><?= $user['points']; ?></span></td>
                    <td><?= $user['time']; ?></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <div class="alert alert-warning">
            There are no learners who have finished this course.
        </div>
    <?php endif; ?>
</div> <!-- /container -->
