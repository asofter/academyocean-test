<?php
ini_set('memory_limit', '1024M');
ini_set('upload_max_filesize', '9M');
header_remove('X-Powered-By');

ini_set('display_errors', 1);
error_reporting(E_ALL ^ E_NOTICE ^ E_STRICT);

if($_SERVER['HTTP_HOST'] == 'lesson.dev' || isset($_GET['__lesson_debug'])) {
    defined('YII_DEBUG') or define('YII_DEBUG', true);
} else {
    defined('YII_DEBUG') or define('YII_DEBUG', false);
}

defined('YII_ENV') or define('YII_ENV', 'prod');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/../config/web.php');

(new yii\web\Application($config))->run();
